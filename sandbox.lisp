;; 
;; SMTLINK TUTORIAL
;;

(in-package "ACL2")
(include-book "projects/smtlink/top" :dir :system)
(tshell-ensure)
(add-default-hints '((SMT::SMT-computed-hint clause)))


;; EXAMPLE 1

(defun x^2-y^2 (x y)
 (- (* x x) (* y y)))

(defthm poly-ineq-example
 (implies (and (realp x)
	       (realp y)
	       (<= (+ (* (/ 9 8) x x) (* y y))
		   1)
	       (<= (x^2-y^2 x y)
		   1))
	  (< y 
	     (- (* 3 (- x (/ 17 8)) (- x (/ 17 8)))
		3)))
 :hints (("GOAL" :smtlink nil)))


;; EXAMPLE 2

(defun my-smtlink-expt-config
 nil (declare (xargs :guard t))
 (smt::change-smtlink-config (smt::default-smt-cnf)
			     :smt-module "RewriteExpt"
			     :smt-class "to_smt_w_expt"
			     :smt-cmd "python"
			     :pythonpath ""))

(defattach smt::custom-smt-cnf my-smtlink-expt-config)

(defun x^2+y^2 (x y)
 (+ (* x x) (* y y)))

(encapsulate 
 nil 
 (local (include-book "arithmetic-5/top" :dir :system))
 
 (defthm poly-of-expt-example 
  (implies (and (realp x) (realp y) (realp z)
		(integerp m) (integerp n)
		(< 0 z) (< z 1)
		(< 0 m) (< m n))
	   (<= (* 2 (expt z n) x y)
	       (* (expt z m) (x^2+y^2 x y))))
  :hints (("GOAL" :smtlink-custom (:functions ((expt :formals ((r realp) (i realp))
						     :returns ((ex realp))
						     :level 0))
					      :hypotheses (((< (expt z n) (expt z m)))
							   ((< 0 (expt z m)))
							   ((< 0 (expt z n))))
					      :int-to-rat t)))))






















